const carMkers: string[] = ["ford", "toyota", "chevt"];
const dates = [new Date(), new Date()];
const carByMake = [["f150"], ["corolla"], ["camaro"]];
const carsByMake: string[][] = [];

// help with inference when extraction values
const car = carMkers[0];
const myCar = carMkers.pop();

// prenvent incapatible values
carMkers.push("100");

//help with map
carMkers.map((car: string): string => {
  return car.toLocaleUpperCase();
});

// felexible types
const importantDates: (Date | string)[] = [new Date()];
importantDates.push("2030-10-10");
importantDates.push(new Date());
importantDates.push("er");
