class Vehicule {
  //color: string ;
  constructor(public color: string) {}
  public drive(): void {
    console.log("test test");
  }

  protected honk(): void {
    console.log("beep");
  }
}
const vehicles = new Vehicule("orange");
console.log(vehicles.color);
class Car extends Vehicule {
  constructor(public wheel: number, color: string) {
    super(color);
  }
  public drive(): void {
    console.log("vroom");
  }
  startDrivingProcess(): void {
    this.drive();
  }
}
const vehicle = new Vehicule("red");
const care = new Car(4, "red");
care.startDrivingProcess();
vehicle.drive();
