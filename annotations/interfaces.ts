interface Reportable {
  summary(): string;
}
const oldCivic = {
  name: "civi",
  year: new Date(),
  broken: true,
  summary(): string {
    return `Name: ${this.name}`;
  }
};

const drinks = {
  color: "brown",
  carbonated: true,
  sugar: 40,
  summary(): string {
    return `My drink has ${this.sugar} grame of sugar`;
  }
};
const printSummary = (item: Reportable): void => {
  //console.log(`Name: ${vehicle.name}`);
  //console.log(`Year: ${vehicle.year}`);
  //console.log(`Broken: ${vehicle.broken}`);
  console.log(item.summary());
};

printSummary(oldCivic);
printSummary(drinks);
